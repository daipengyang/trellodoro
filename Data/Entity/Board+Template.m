#import "Board+Template.h"

@interface Board (TemplateInternal)
@property (readonly) NSArray<NSString *> *templateColumnNames;
@end

@implementation Board (Template)
static NSArray<NSString *> *_templateColumnNames;
+ (NSArray<NSString *> *)templateColumnNames {
    if (!_templateColumnNames) {
        _templateColumnNames = @[@"Bucket List",
                                 @"Doing",
                                 @"Done"];
    }
    return _templateColumnNames;
}

- (void)createTemplateColumns {
    for (NSString *name in [self class].templateColumnNames) {
        Column *column = [[Column alloc] initWithContext:self.managedObjectContext];
        column.name = name;
        [self addColumnsObject:column];
    }
}

+ (instancetype)templateBoardWith:(NSString *)name in:(NSManagedObjectContext *)context {
    Board *board = [[Board alloc] initWithContext:context];
    board.name = name;
    [board createTemplateColumns];
    return board;
}
@end
