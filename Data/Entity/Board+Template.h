@import CoreData;
#import <Data/Data-Swift.h>

NS_ASSUME_NONNULL_BEGIN

@interface Board (Template)
+ (instancetype)templateBoardWith:(NSString *)name in:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END
