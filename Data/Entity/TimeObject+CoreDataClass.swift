import Foundation
import CoreData

@objc(TimeObject)
public class TimeObject: NSManagedObject {
    public override func awakeFromInsert() {
        super.awakeFromInsert()

        let now = Date()
        createdAt = now
        updatedAt = now
    }
    
    public override func willSave() {
        super.willSave()
        
        guard
            !isDeleted,
            !changedValues().keys.contains("updatedAt") else { return }
        updatedAt = Date()
    }
}
