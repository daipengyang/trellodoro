#import "Column+Next.h"

@implementation Column (Next)
- (Column *)next {
    NSArray<Column *> *columns = self.inBoard.columns.array;
    NSUInteger index = [columns indexOfObject:self];
    @try {
        return columns[index + 1];
    } @catch (NSException *exception) {
        return nil;
    }
}
@end
