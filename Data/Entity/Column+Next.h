#import "Data/Data-Swift.h"

@interface Column (Next)
/// Next column in the same board.
@property (readonly, nullable) Column *next;
@end
