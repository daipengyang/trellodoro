import CoreData

/**
 This subclass is the hint which Bundle for `NSPersistentContainer` to search `mom` in.
 
 > NSPersistentContainer knows which target(bundle) it is subclassed and search the mom in such bundle.
 > See [WWDC2018 Session224 Core Data Best Practices](https://developer.apple.com/videos/play/wwdc2018-224/?time=252)
 */
open class PersistentContainer: NSPersistentContainer {
    open class var `default`: PersistentContainer {
        return defaultContainer
    }
    
    /// Core Data failed to locate correct Bundle to look up mom as described in class doc.
    /// Override `convenience init(name:)` to provide the feature
    public convenience init(name: String) {
        guard
            let bundle = Bundle(identifier: "tw.idv.pengyang.dai.Trellodoro.Data"),
            let momURL = bundle.url(forResource: "Model", withExtension: "momd"),
            let mom = NSManagedObjectModel(contentsOf: momURL) else {
                fatalError()
        }
        self.init(name: name, managedObjectModel: mom)
    }
    
    static private let defaultContainer: PersistentContainer = {
        let result = PersistentContainer(name: "Data")
        result.loadPersistentStores { _, error in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        }
        return result
    }()
}

// TODO: Subclass managed objects to provide NSOrderedSet generic
