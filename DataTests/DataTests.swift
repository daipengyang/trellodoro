import XCTest
import CoreData
@testable import Data

class DataTests: XCTestCase {
    private var containers = [PersistentContainer]()
    func newContainer() -> PersistentContainer {
        let container = PersistentContainer.random
        containers.append(container)
        return container
    }
    
    override func tearDown() {
        super.tearDown()
        containers
            .forEach { try! $0.persistentStoreCoordinator.removeAllStores(in: .default) }
    }
    
    func newColumnBoard(in context: NSManagedObjectContext) -> (column: Column, board: Board) {
        let column = Column(context: context)
        column.name = UUID().uuidString
        
        let board = Board(context: context)
        board.name = UUID().uuidString
        
        column.inBoard = board
        
        return (column: column, board: board)
    }
}

final class ColumnTests: DataTests {
    func test_denyDeletion_whenContainAnyTask() {
        let context = newContainer().viewContext
        let entities = newColumnBoard(in: context)
        let task = Task(context: context)
        task.name = UUID().uuidString
        task.column = entities.column
        try! context.save()
        
        context.delete(entities.column)
        
        XCTAssertThrowsError(try context.save())
    }
}

final class TimeObjectTests: DataTests {
    let sameDateComparisonOffset: TimeInterval = 0.01
    
    func test_createdAtIsSet_whenCreate() {
        let context = newContainer().viewContext
        
        let timeObject = newColumnBoard(in: context).board as TimeObject
        let date = Date()
        
        guard let createdAt = timeObject.createdAt else {
            return XCTFail("TimeObject.createdAt should not be nil.")
        }
        XCTAssert(date.timeIntervalSince(createdAt) < sameDateComparisonOffset)
        XCTAssertNoThrow(try context.save())
    }
    
    func test_updatedAtIsSet_whenCreate() {
        let context = newContainer().viewContext
        
        let timeObject = newColumnBoard(in: context).board as TimeObject
        let date = Date()
        
        guard let updatedAt = timeObject.updatedAt else {
            return XCTFail("TimeObject.updatedAt should not be nil.")
        }
        XCTAssert(date.timeIntervalSince(updatedAt) < sameDateComparisonOffset)
        XCTAssertNoThrow(try context.save())
    }
    
    func test_updatedAtIsUpdated_whenUpdateAndSave() {
        let context = newContainer().viewContext
        let timeObject = newColumnBoard(in: context).board
        XCTAssertNoThrow(try context.save())
        _ = XCTWaiter.wait(for: [XCTestExpectation()], timeout: sameDateComparisonOffset)
        timeObject.name = UUID().uuidString
        
        XCTAssertNoThrow(try context.save())
        
        guard let updatedAt = timeObject.updatedAt else {
            return XCTFail("TimeObject.updatedAt should not be nil.")
        }
        XCTAssert(Date().timeIntervalSince(updatedAt) < sameDateComparisonOffset)
    }
    
}

private extension PersistentContainer {
    static var random: PersistentContainer {
        let result = PersistentContainer(name: UUID().uuidString)
        result.loadPersistentStores { _, error in
            if let error = error {
                XCTFail("Failed to load persistent stores in PersistentContainer for eason: \(error)")
            }
        }
        return result
    }
}
