import Foundation
import CoreData

extension NSPersistentStoreCoordinator {
    func remove(_ store: NSPersistentStore, in fileManager: FileManager) throws {
        try remove(store)
        
        guard let url = store.url else { return }
        try fileManager
            .contentsOfDirectory(at: url.deletingLastPathComponent(),
                                 includingPropertiesForKeys: nil,
                                 options: [.skipsPackageDescendants, .skipsSubdirectoryDescendants])
            .filter { url.lastPathComponentWithoutExtension == $0.lastPathComponentWithoutExtension }
            .forEach { try fileManager.removeItem(at: $0) }
        
    }
    
    func removeAllStores(in fileManager: FileManager) throws {
        try persistentStores
            .forEach { try remove($0, in: fileManager) }
    }
}

extension URL {
    var lastPathComponentWithoutExtension: String {
        return deletingPathExtension().lastPathComponent
    }
}
