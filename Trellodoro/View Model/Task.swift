import Foundation
import Data

extension Task {
    var pomodoroVisualizedDescription: String {
        let spend = pomodoroSpent?.intValue ?? 0
        let estimated = pomodoroEstimated?.intValue ?? 0
        
        let description = estimated > spend ?
            Array(repeating: "●", count: spend) + Array(repeating: "○", count: estimated - spend) :
            Array(repeating: "●", count: spend)
        return description.joined()
    }
}
