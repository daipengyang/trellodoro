import UIKit
import Data

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        guard
            let navgiationController = window?.rootViewController as? UINavigationController,
            let boardContentScene = navgiationController.topViewController as? BoardContentScene else {
                assertionFailure()
                return false
        }
        boardContentScene.board = defaultBoard
        
        return true
    }
    
    var defaultBoard: Board {
        let context = PersistentContainer.default.viewContext
        guard
            let boards = try? context.fetch(Board.fetchRequest()) as? [Board],
            let board = boards?.first else {
                return Board.templateBoard(with: "Trellodoro", in: context)
        }
        return board
    }
}
