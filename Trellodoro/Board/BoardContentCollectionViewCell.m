#import "BoardContentCollectionViewCell.h"
#import "Trellodoro-Swift.h"

@implementation BoardContentCollectionViewCell
// MARK: Life Cycle
- (void)prepareForReuse {
    [super prepareForReuse];
    self.embedColumnViewController = nil;
}

// MARK: Property
- (void)setEmbedColumnViewController:(UIViewController *)embedColumnViewController {
    if (_embedColumnViewController != embedColumnViewController) {
        [_embedColumnViewController willMoveToParentViewController:nil];
        [_embedColumnViewController.view removeFromSuperview];
        [_embedColumnViewController removeFromParentViewController];
    }
    
    [embedColumnViewController willMoveToParentViewController:embedColumnViewController.parentViewController];
    
    UIView *embedView = embedColumnViewController.view;
    if (embedView) [self addSubview:embedView];
    embedView.translatesAutoresizingMaskIntoConstraints = NO;
    [embedView.leftAnchor constraintEqualToAnchor:self.leftAnchor].active = YES;
    [embedView.rightAnchor constraintEqualToAnchor:self.rightAnchor].active = YES;
    [embedView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
    [embedView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    
    [embedColumnViewController didMoveToParentViewController:embedColumnViewController.parentViewController];
    
    _embedColumnViewController = embedColumnViewController;
}
@end
