@import UIKit;

@interface BoardContentCollectionViewCell : UICollectionViewCell
/**
 The setter embed this to cell's view hierarchy and make it fills cell's view.
 Setting this to `nil` removes from cell's view hierarchy.
 @warning Call `UIViewController.addChild(_:)` before setting this.
 @note `UIViewController` embed-notify functions `willMove(toParent:)` and `didMove(toParent:)` are called in this setter. No need to call explicitly.
 */
@property (nonatomic, nullable, weak) UIViewController *embedColumnViewController;
@end
