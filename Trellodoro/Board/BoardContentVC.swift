import UIKit
import CoreData
import Data

protocol BoardContentScene: AnyObject {
    var board: Board! { get set }
}

final class BoardContentVC: UICollectionViewController, BoardContentScene {
    var board: Board! {
        didSet {
            title = board.name
            if isViewLoaded {
                collectionView.reloadData()
            }
        }
    }
    private var viewContext: NSManagedObjectContext! {
        return board.managedObjectContext
    }
    
    // TODO: Opt for NSFetchedResultsController for indexPath - managedObject conversion
    /// Access the column according to current sort/filter
    func column(at indexPath: IndexPath) -> Column {
        guard let columns = board.columns?.array as? [Column] else { fatalError() }
        return columns[indexPath.item]
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(board != nil)
    }
}

// MARK: - Action
extension BoardContentVC {
    @IBAction func addColumn() {
        let alertController = UIAlertController(title: "Add Column Name:", message: nil) { [unowned self] name in
            let column = Column(context: self.viewContext)
            let insertedIndexPath = IndexPath(item: 0, section: 0)
            self.board.insertIntoColumns(column, at: insertedIndexPath.item)
            column.name = name
            try? self.viewContext.save()
            self.collectionView.insertItems(at: [insertedIndexPath])
        }
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Collection View
// MARK: Data Source
extension BoardContentVC {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return board.columns?.count ?? 0
    }
    
    static let cellIdentifier = "ColumnCell"
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dequeuedCell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: self).cellIdentifier, for: indexPath) //swiftlint:disable:this line_length
        guard
            let navigationController = UIStoryboard(name: "Column", bundle: nil).instantiateInitialViewController() as? UINavigationController, //swiftlint:disable:this line_length
            let columnContentVC = navigationController.topViewController as? ColumnContentVC,
            let cell = dequeuedCell as? BoardContentCollectionViewCell else {
                assertionFailure()
                return dequeuedCell
        }
        columnContentVC.column = column(at: indexPath)
        columnContentVC.delegate = self
        
        addChild(navigationController)
        cell.embedColumnViewController = navigationController
        
        return cell
    }
}

// MARK: Move
extension BoardContentVC {
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 moveItemAt sourceIndexPath: IndexPath,
                                 to destinationIndexPath: IndexPath) {
        let sourceColumn = column(at: sourceIndexPath)
        board.removeFromColumns(sourceColumn)
        board.insertIntoColumns(sourceColumn, at: destinationIndexPath.row)
        try? viewContext.save()
    }
}

// MARK: - Column Content Scene
extension BoardContentVC: ColumnContentSceneDelegate {
    func columnContent(columnContent: ColumnContentVC, didRemoveColumn column: Column) {
        let tartgetCells = collectionView.visibleCells.filter {
            guard let cell = $0 as? BoardContentCollectionViewCell else { return false }
            return columnContent.navigationController === cell.embedColumnViewController
        }
        
        if
            let targetCell = tartgetCells.first,
            let indexPath = collectionView.indexPath(for: targetCell) {
            collectionView.deleteItems(at: [indexPath])
        }
    }
    
    func columnContent(columnContent: ColumnContentVC, didMoveTaskToNewBoard task: Task, from column: Column) {
        let targetColumnContentScnenes = children
            .compactMap { ($0 as? UINavigationController)?.viewControllers.first as? ColumnContentVC }
            .filter { $0.column == task.column }
        
        targetColumnContentScnenes.forEach {
            guard let indexPath = $0.indexPath(for: task) else { return }
            $0.tableView.insertRows(at: [indexPath], with: .left)
        }
        
        assert(targetColumnContentScnenes.count == 1)
    }
}
