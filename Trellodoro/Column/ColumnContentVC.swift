import UIKit
import CoreData
import Data

// TODO: Opt for NSFetchedResultsController for data driven view update
@objc protocol ColumnContentSceneDelegate {
    @objc optional func columnContent(columnContent: ColumnContentVC,
                                      willMoveTaskToNewBoard task: Task, from column: Column)
    @objc optional func columnContent(columnContent: ColumnContentVC,
                                      didMoveTaskToNewBoard task: Task, from column: Column)
    @objc optional func columnContent(columnContent: ColumnContentVC, willRemoveColumn column: Column)
    @objc optional func columnContent(columnContent: ColumnContentVC, didRemoveColumn column: Column)
}

final class ColumnContentVC: UITableViewController {
    @IBOutlet private var renameBarButtonItem: UIBarButtonItem!
    @IBOutlet private var deleteBarButtonItem: UIBarButtonItem!
    
    weak var delegate: ColumnContentSceneDelegate?
    
    // TODO: Abstract Entity Manager to manipulate and save to context for view controller
    var column: Column! {
        didSet {
            title = column.name
            if isViewLoaded {
                tableView.reloadData()
            }
        }
    }
    private var viewContext: NSManagedObjectContext! {
        return column.managedObjectContext
    }
    
    // TODO: Opt for NSFetchedResultsController for indexPath - managedObject conversion
    /// Access the task according to current sort/filter
    func task(at indexPath: IndexPath) -> Task {
        guard let tasks = column.tasks?.array as? [Task] else { fatalError() }
        return tasks[indexPath.row]
    }
    
    /// Access the indexPath for column
    func indexPath(for task: Task) -> IndexPath? {
        guard let tasks = column.tasks?.array as? [Task] else { fatalError() }
        guard let index = tasks.firstIndex(of: task) else {
            return nil
        }
        return IndexPath(item: index, section: 0)
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(column != nil)
        navigationItem.rightBarButtonItem = editButtonItem
    }
    
    // MARK: Edit
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        navigationItem.setLeftBarButton(
            editing ? renameBarButtonItem : nil,
            animated: true)
        
        navigationItem.setRightBarButtonItems(
            editing ? [editButtonItem, deleteBarButtonItem] : [editButtonItem],
            animated: true)
    }
}

// MARK: - Table View
// MARK: Data Source
extension ColumnContentVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return column.tasks?.count ?? 0
    }
    
    static let cellIdentifier = "TaskCell"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: self).cellIdentifier, for: indexPath)
        let task = self.task(at: indexPath)
        
        cell.textLabel?.text = task.name
        cell.detailTextLabel?.text = task.pomodoroVisualizedDescription
        
        return cell
    }
}

// MARK: Move
extension ColumnContentVC {
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView,
                            moveRowAt sourceIndexPath: IndexPath,
                            to destinationIndexPath: IndexPath) {
        let sourceTask = task(at: sourceIndexPath)
        column.removeFromTasks(sourceTask)
        column.insertIntoTasks(sourceTask, at: destinationIndexPath.row)
        try? viewContext.save()
    }
}

// MARK: Swipe Action
extension ColumnContentVC {
    override func tableView(_ tableView: UITableView,
                            leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? { //swiftlint:disable:this line_length
        let actions = [
            UIContextualAction(style: .normal, title: "Add a pomodoro") { _, _, completion in
                let task = self.task(at: indexPath)
                let spend = task.pomodoroSpent?.intValue ?? 0
                let estimated = task.pomodoroEstimated?.intValue ?? 0
                
                task.pomodoroEstimated = spend > estimated ?
                    task.pomodoroSpent?.adding(1) :
                    task.pomodoroEstimated?.adding(1)
                try? self.viewContext.save()
                completion(true)
                tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.right)
            },
            UIContextualAction(style: .normal, title: "Finish a pomodoro") { _, _, completion in
                let task = self.task(at: indexPath)
                task.pomodoroSpent = task.pomodoroSpent?.adding(1)
                try? self.viewContext.save()
                completion(true)
                tableView.reloadRows(at: [indexPath], with: .none)
            }
        ]
        
        return UISwipeActionsConfiguration(actions: actions)
    }
    
    override func tableView(_ tableView: UITableView,
                            trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? { //swiftlint:disable:this line_length
        let actions = [
            UIContextualAction(style: .normal, title: "Move to Next") { _, _, completion in
                guard let nextColumn = self.column.next else {
                    return completion(false)
                }
                let task = self.task(at: indexPath)
                
                self.delegate?.columnContent?(columnContent: self, willMoveTaskToNewBoard: task, from: self.column)
                
                task.column = nextColumn
                try? self.viewContext.save()
                completion(true)
                tableView.deleteRows(at: [indexPath], with: .right)
                
                self.delegate?.columnContent?(columnContent: self, didMoveTaskToNewBoard: task, from: self.column)
            },
            UIContextualAction(style: .destructive, title: "Remove") { _, _, completion in
                let task = self.task(at: indexPath)
                self.viewContext.delete(task)
                try? self.viewContext.save()
                completion(true)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        ]
        
        return UISwipeActionsConfiguration(actions: actions)
    }
}

// MARK: - Action
extension ColumnContentVC {
    @IBAction func addTask() {
        let alertController = UIAlertController(title: "Add Task Name", message: nil) { [unowned self] in
            let task = Task(context: self.viewContext)
            task.name = $0
            task.column = self.column
            try? self.viewContext.save()
            self.tableView.reloadData()
        }
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func renameColumn() {
        let alertController = UIAlertController(title: "Rename Column", message: nil) { [unowned self] in
            self.column.name = $0
            try? self.viewContext.save()
            self.title = self.column.name
        }
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func removeColumn() {
        guard let tasks = column.tasks?.array as? [Task] else { return assertionFailure() }
        
        func removeAndSave() {
            self.delegate?.columnContent?(columnContent: self, willRemoveColumn: self.column)
            
            tasks.forEach { self.viewContext.delete($0) }
            self.viewContext.delete(self.column)
            try? self.viewContext.save()
            
            self.delegate?.columnContent?(columnContent: self, didRemoveColumn: self.column)
        }
        
        if !tasks.isEmpty {
            let alertController = UIAlertController(
                title: "Delete Column",
                message: "All tasks in the column \(column.name?.description ?? "") will be deleted, are you sure?",
                preferredStyle: .alert, completion: removeAndSave)
            present(alertController, animated: true, completion: nil)
        } else {
            removeAndSave()
        }
    }
}
