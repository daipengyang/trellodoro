import UIKit

extension UIAlertController {
    typealias VoidClosure = (() -> Void)
    
    convenience init(title: String?, message: String?,
                     configure: ((UITextField) -> Void)? = nil,
                     cancel: VoidClosure? = nil,
                     completion: @escaping (String?) -> Void) {
        self.init(title: title, message: message, preferredStyle: .alert)
        var textField: UITextField!
        addTextField {
            configure?($0)
            textField = $0
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { _ in cancel?() }
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in completion(textField.text) }
        [okAction, cancelAction].forEach { addAction($0) }
        preferredAction = okAction
    }
    
    convenience init(title: String?, message: String?, preferredStyle: Style,
                     cancel: VoidClosure? = nil,
                     completion: VoidClosure?) {
        self.init(title: title, message: message, preferredStyle: preferredStyle)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { _ in cancel?() }
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in completion?() }
        [okAction, cancelAction].forEach { addAction($0) }
        preferredAction = okAction
    }
}
